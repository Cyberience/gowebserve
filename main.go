package main

import (
	"gowebserve/src/conf"
	"gowebserve/src/db"
	"gowebserve/src/lib"
	"gowebserve/src/route"
	"time"
)

/*
ToDo:
*/

/***********************_**************
 *                     (_)
 *      _ __ ___   __ _ _ _ __
 *     | '_ ` _ \ / _` | | '_ \
 *     | | | | | | (_| | | | | |
 *     |_| |_| |_|\__,_|_|_| |_|
 * ---------------------------------- */
func main() {
	for { //In case of failure, try again in 10 seconds.
		db.Init("db/WebDB-v1")
		lib.LogInit(conf.Val.DEBUG, conf.Val.AppName)
		route.InitRoute()
		time.Sleep(time.Duration(10) * time.Second)
	}
}
