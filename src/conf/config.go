package conf

import (
	"flag"
	"fmt"
	"gowebserve/src/lib"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/joho/godotenv"
)

type Config struct {
	configFilePath string
	DEBUG          string
	VERSION        string
	AppName        string
	PORT           string
	W_SQL_DB       string
	THROTTLE       int
}
type Status struct {
	Running  int
	ErrorMsg string
}

/***********************************
 *                     _
 *                    | |
 *      _ __ ___ _ __ | |_   _
 *     | '__/ _ \ '_ \| | | | |
 *     | | |  __/ |_) | | |_| |
 *     |_|  \___| .__/|_|\__, |
 *              | |       __/ |
 *              |_|      |___/
 * * * * * * * * * * * * * * * * * *
 * Define the reply structure
 * ------------------------------- */
type Reply struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
}

var (
	reply  Reply
	Val    Config
	status Status
)

func init() {
	defer func() {
		r := recover()
		if r != nil {
			fmt.Println("Possible .env error:", r)
		}
	}()
	status = Status{1, "Starting"}
	flag.StringVar(&Val.configFilePath, "c", "config/config.env", "config file path")
	flag.Parse()
	if //noinspection GoUnresolvedReference
	nil != godotenv.Load(Val.configFilePath) {
		fmt.Println("Environment File not found:", Val.configFilePath)
	} else {
		fmt.Println("INFO", "Config File Loaded:", Val.configFilePath)
	}

	Val = Config{Val.configFilePath,
		getEnv("DEBUG", "ERROR DEBUG INFO CRIT STDOUT"),
		getEnv("VERSION", "0.0.1"),
		getEnv("APPNAME", filepath.Base(os.Args[0])),
		getEnv("PORT", "8080"),
		getEnv("W_SQL_DB", "./db/main.db"),
		getEnvAsInt("THROTTLE", 1000),
	}
	fmt.Println("Listening on port:", Val.PORT)
}

// Simple helper function to read an environment or return a default value
func getEnv(key string, defaultVal string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	}
	return defaultVal
}

// Helper to read an environment variable into a bool or return default value
func getEnvAsBool(name string, defaultVal bool) bool {
	valStr := getEnv(name, "")
	if val, err := strconv.ParseBool(valStr); err == nil {
		return val
	}

	return defaultVal
}

// Simple helper function to read an environment variable into integer or return a default value
func getEnvAsInt(name string, defaultVal int) int {
	valueStr := getEnv(name, "")
	if value, err := strconv.Atoi(valueStr); err == nil {
		return value
	}
	return defaultVal
}

// Simple helper function to read an environment variable into integer or return a default value
func getEnvAsDuration(name string, defaultVal time.Duration) time.Duration {
	valueStr := getEnv(name, "")
	if value, err := strconv.Atoi(valueStr); err == nil {
		retVal := time.Duration(value)
		return retVal
	}
	return defaultVal
}

// Simple helper function to read an environment variable into integer or return a default value
func getEnvAsInt64(name string, defaultVal int64) int64 {
	valueStr := getEnv(name, "")
	if value, err := strconv.ParseInt(valueStr, 10, 64); err == nil {
		return value
	}

	return defaultVal
}

//  ----------------------------------------------------------------
//              _   _   _                 _   _                _
//             | | | | | |               | | | |              | |
//    __ _  ___| |_| |_| | ___  __ _ _ __| |_| |__   ___  __ _| |_
//   / _` |/ _ \ __|  _  |/ _ \/ _` | '__| __| '_ \ / _ \/ _` | __|
//  | (_| |  __/ |_| | | |  __/ (_| | |  | |_| |_) |  __/ (_| | |_
//   \__, |\___|\__\_| |_/\___|\__,_|_|   \__|_.__/ \___|\__,_|\__|
//    __/ |
//   |___/
func nextHeartBeat() (nextHeartBeat int) {
	nextHeartBeat = Val.THROTTLE
	if strings.Contains(Val.DEBUG, "DEBUG") {
		nextHeartBeat = lib.RandomRange(nextHeartBeat/6, nextHeartBeat/2)
	} else {
		nextHeartBeat = lib.RandomRange(nextHeartBeat/2, nextHeartBeat*2)
	}
	return
}
