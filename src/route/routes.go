package route

import (
	"expvar"
	"flag"
	"fmt"
	"gowebserve/src/conf"
	"gowebserve/src/lib"

	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttp/expvarhandler"
)

var (
	addr               = flag.String("addr", "localhost:8080", "TCP address to listen to")
	addrTLS            = flag.String("addrTLS", "", "TCP address to listen to TLS (aka SSL or HTTPS) requests. Leave empty for disabling TLS")
	byteRange          = flag.Bool("byteRange", false, "Enables byte range requests if set to true")
	certFile           = flag.String("certFile", "./ssl-cert-snakeoil.pem", "Path to TLS certificate file")
	compress           = flag.Bool("compress", false, "Enables transparent response compression if set to true")
	dir                = flag.String("dir", "./http", "Directory to serve static files from")
	generateIndexPages = flag.Bool("generateIndexPages", true, "Whether to generate directory index pages")
	keyFile            = flag.String("keyFile", "./ssl-cert-snakeoil.key", "Path to TLS key file")
	vhost              = flag.Bool("vhost", false, "Enables virtual hosting by prepending the requested path with the requested hostname")

	// Counter for total number of fs calls
	fsCalls = expvar.NewInt("fsCalls")

	// Counters for various response status codes
	fsOKResponses          = expvar.NewInt("fsOKResponses")
	fsNotModifiedResponses = expvar.NewInt("fsNotModifiedResponses")
	fsNotFoundResponses    = expvar.NewInt("fsNotFoundResponses")
	fsOtherResponses       = expvar.NewInt("fsOtherResponses")

	// Total size in bytes for OK response bodies served.
	fsResponseBodyBytes = expvar.NewInt("fsResponseBodyBytes")
)

//   **********************************************
//    _____       _ _   _____             _
//   |_   _|     (_) | |  __ \           | |
//     | |  _ __  _| |_| |__) |___  _   _| |_ ___
//     | | | '_ \| | __|  _  // _ \| | | | __/ _ \
//    _| |_| | | | | |_| | \ \ (_) | |_| | ||  __/
//   |_____|_| |_|_|\__|_|  \_\___/ \__,_|\__\___|
func InitRoute() {
	fs := &fasthttp.FS{
		Root:               *dir,
		IndexNames:         []string{"index.html"},
		GenerateIndexPages: *generateIndexPages,
		Compress:           *compress,
		AcceptByteRange:    *byteRange,
	}
	if *vhost {
		fs.PathRewrite = fasthttp.NewVHostPathRewriter(0)
	}
	fsHandler := fs.NewRequestHandler()

	//   ____   ____   _  _   _____  ___    ___
	//  /  _ \ / __ \ ) () ( )__ __() __(  (  _(
	//  )  ' / ))__(( | \/ |   | |  | _)   _) \
	//  |_()_\ \____/ )____(   )_(  )___( )____)
	//
	requestHandler := func(ctx *fasthttp.RequestCtx) {
		switch string(ctx.Path()) {
		case "/stats", "/status": // /stats output may be filtered using regexps. For example:
			expvarhandler.ExpvarHandler(ctx) //   * /stats?r=fs will show only stats (expvars) containing 'fs'  in their names.
		case "/login": // /stats output may be filtered using regexps. For example:
		case "/register": // /stats output may be filtered using regexps. For example:
		default:
			fsHandler(ctx)
			updateFSCounters(ctx)
		}
	}

	//    ___   ___   ____   _   _  ___     _  _   _____  _____  ____
	//   (  _( ) __( /  _ \ \ ( ) /) __(   ) () ( )__ __()__ __()  _)\
	//   _) \  | _)  )  ' /  )\_/( | _)    | -- |   | |    | |  | '__/
	//  )____) )___( |_()_\   \_/  )___(   )_()_(   )_(    )_(  )_(
	if len(*addr) > 0 {
		go func() {
			lib.Info("Starting HTTP server on ", *addr)
			if err := fasthttp.ListenAndServe(*addr, requestHandler); err != nil {
				lib.Error("error in ListenAndServe: %s", err)
				lib.Alive = false
			}
		}()
	}

	//    ___   ___   ____   _   _  ___     _  _   _____  _____  ____    ___
	//   (  _( ) __( /  _ \ \ ( ) /) __(   ) () ( )__ __()__ __()  _)\  (  _(
	//   _) \  | _)  )  ' /  )\_/( | _)    | -- |   | |    | |  | '__/  _) \
	//  )____) )___( |_()_\   \_/  )___(   )_()_(   )_(    )_(  )_(    )____)
	if len(*addrTLS) > 0 {
		go func() {
			lib.Info("Starting HTTPS server on ", *addrTLS)
			if err := fasthttp.ListenAndServeTLS(*addrTLS, *certFile, *keyFile, requestHandler); err != nil {
				lib.Error("error in ListenAndServeTLS: %s", err)
				lib.Alive = false
			}
		}()
	}

	lib.Info("Serving files from directory %q", *dir, "See stats at http://", *addr, "/stats")
	lib.Forever()
}

func updateFSCounters(ctx *fasthttp.RequestCtx) {
	// Increment the number of fsHandler calls.
	fsCalls.Add(1)

	// Update other stats counters
	resp := &ctx.Response
	switch resp.StatusCode() {
	case fasthttp.StatusOK:
		fsOKResponses.Add(1)
		fsResponseBodyBytes.Add(int64(resp.Header.ContentLength()))
	case fasthttp.StatusNotModified:
		fsNotModifiedResponses.Add(1)
	case fasthttp.StatusNotFound:
		fsNotFoundResponses.Add(1)
	default:
		fsOtherResponses.Add(1)
	}
}

//   -_____----------------------------_-----_----_--------------------------------
//   |  __ \                          | |   | |  | |               | | |
//   | |__) |___  __ _ _   _  ___  ___| |_  | |__| | __ _ _ __   __| | | ___ _ __
//   |  _  // _ \/ _` | | | |/ _ \/ __| __| |  __  |/ _` | '_ \ / _` | |/ _ \ '__|
//   | | \ \  __/ (_| | |_| |  __/\__ \ |_  | |  | | (_| | | | | (_| | |  __/ |
//   |_|  \_\___|\__, |\__,_|\___||___/\__| |_|  |_|\__,_|_| |_|\__,_|_|\___|_|
//                  | |
//                  |_|
func requestLogin(ctx *fasthttp.RequestCtx) {
	defer func() {
		r := recover()
		if r != nil {
			lib.Error("Request Handler Failed:", r)
		}
	}()
	if lib.ThrottleAllow(ctx.RemoteIP().String(), conf.Val.THROTTLE) { //At least some throttle control, how ever small to prevent overload.
		lib.Info("Login Form Requested")
	}
}

func requestRegister(ctx *fasthttp.RequestCtx) {
	defer func() {
		r := recover()
		if r != nil {
			lib.Error("Request Handler Failed:", r)
		}
	}()
	if lib.ThrottleAllow(ctx.RemoteIP().String(), conf.Val.THROTTLE) { //At least some throttle control, how ever small to prevent overload.
		lib.Info("Register Form Requested")
	}
}

/*****************************************************
 *      _ __ ___  ___ _ __   ___  _ __  ___  ___
 *     | '__/ _ \/ __| '_ \ / _ \| '_ \/ __|/ _ \
 *     | | |  __/\__ \ |_) | (_) | | | \__ \  __/
 *     |_|  \___||___/ .__/ \___/|_| |_|___/\___|
 *                   | |
 *                   |_|
 * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Package a proper response to the caller.
 * ------------------------------------------------- */
func response(ctx *fasthttp.RequestCtx, reply conf.Reply) {
	if reply.Code < 300 {
		_, _ = fmt.Fprint(ctx, reply.Msg)
		ctx.SetStatusCode(reply.Code)
	} else {
		ctx.Error(reply.Msg, reply.Code)
		ctx.Response.ImmediateHeaderFlush = true
	}
}

type User struct {
	Email  string `json:"email"`
	Secret string `json:"secret"`
}

var user User

func doLoginHandler(ctx *fasthttp.RequestCtx) {
	// Create a new instance of Bird
	err := ctx.Request.PostArgs()

	if err != nil { // In case of any error, we respond with an error to the user
		fmt.Println(fmt.Errorf("Error: %v", err))
		//ctx.Response.WriteTo(http.StatusInternalServerError)
		return
	}

	//user.Email = ctx.Request.Form.Get("email")   // Get the information from login page
	//user.Secret = r.Form.Get("secret") //password

	//http.Redirect("/http/", http.StatusFound)
}
