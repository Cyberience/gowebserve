package db

// Todo: Version the Database
import (
	"database/sql"
	"gowebserve/src/lib"

	_ "github.com/mattn/go-sqlite3"
)

var (
	dbLite   *sql.DB
	write_db string
)

func Init(write_db string) {
	defer func() {
		r := recover()
		if r != nil {
			lib.Error("initDB Error:", r)
		}
	}()
	dbLite, _ = sql.Open("sqlite3", write_db)
}
