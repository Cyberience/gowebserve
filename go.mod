module gowebserve

go 1.18

require (
	github.com/joho/godotenv v1.4.0
	github.com/mattn/go-sqlite3 v1.14.12
	github.com/valyala/fasthttp v1.36.0
)

require (
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/klauspost/compress v1.15.3 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
)
